# polygon

## Description
A small program for greating polygon based drawings/images. A plugin system is used to implement different tools, and these are combined in toolchains described in a .JSON file.

The image below is generated with the default toolchain `delaunay_UWQHD`:
```
python polygon.py delaunay_UWQHD -o examples/UWQHD.png
```
![](examples/UWQHD.png)

The toolchain looks like so:
```json
    {
        "name": "delaunay_UWQHD",
        "drawing_size": [3440, 1440],
        "chain": [
            {
                "type": "plotter", 
                "name":"delaunay", 
                "arguments": [300, 100]
            },
            {
                "type": "painter", 
                "name":"gradient", 
                "setup": [
                    [
                        [1720,100], 
                        [3440,1440]
                    ],
                    [
                        [96,32,96],
                        [16,0,12]
                    ],
                    "polar"
                ]
            },
            {
                "type": "plugin", 
                "name": "random", 
                "at": "modify_fill_color", 
                "method": "randomize_color", 
                "arguments": [
                    [100, 1]
                ]
            },
            {
                "type": "saver", 
                "name":"inkscape-png"
            }
        ]
    }
```
It uses the `delaunay` plotting plugin to generate the triangles using [Delaunay triangulation](https://en.wikipedia.org/wiki/Delaunay_triangulation). Then these triangles are added to an SVG drawing and each is painted using a `pole gradient` from the `gradient` plugin. The hue and lightness of the color is then randomly changed by $\pm 1$ to give a little texture. Lastly the SVG gets converted using Inkscape to PNG using the `inkscape-png` plugin.

## Installation
### Requirements
The core program requires
- python3
- svgwrite

Furthermore the plugins requires
- numpy (painter_gradient)
- scipy (painter_gradient, plotter_delaunay)
- pillow (painter_png)
- Inkscape (saver_inkscape-png)

## Usage
Either edit the `toolchains.json` or create a new JSON file for your toolchain(s) and run the program with
```
python polygon.py [TOOLCHAIN NAME] (-c [CUSTOM TOOLCHAINS FILE]) -o [OUTPUT FILE]
```
### Placeholders
Placeholders can be used inside the toolchains file.

A placeholder is a string on the form `"$(expression)"`. The expression is space seperated and uses postfix notation. The following operators are available:
- `+`, `-`, `*`, and `/` are the addition, subtraction, multiplication, and division.
- `%` is a percentage operator and the result is cast to an int. E.g. `$(30 5 %)` evaluates to `1`.
- `%f` same as above without the int casting, e.g. `$(30 5 %f)` evaluates to `1.5`.

Command line arguments can be passed to the placeholders using `@index` inside a placeholder. E.g. `$(@0)` 

#### Examples
##### Simple calculations
Calculations are done using postfix notation. Some examples of this are
```
1 2 + 
= 3

2 4 *
= 8

9 3 /
= 3
```

Multiple operations can also be combined
```
1 2 4 + +
= 1 6 +
= 7

10 3 5 * +
= 10 15 +
= 25
```

##### Multiple values
If multiple values are on the stack after it gets evaluated, it is handled as a list
```
10 20
= [10, 20]


10 5 + 2 2 *
= [15, 4]
```

##### Accessing the command line arguments
Command line arguments that doesn't get parsed normally are accessible using values in placeholders starting with `@`.
```
> python polygon.py [toolchain] -o [file] 1000 500
```

```
@1
= 1000

@2
= 500

@1 @2
= 1000 500
= [1000, 500]

@1 @2 +
= 1500
```

##### Using it together
An example of using the placeholders is the following toolchain
```json
[
    {
        "name": "delaunay_placeholder",
        "drawing_size": "$(@1 @2)",
        "chain": [
            {
                "type": "plotter", 
                "name":"delaunay", 
                "arguments": [300, 100]
            },
            {
                "type": "painter", 
                "name":"gradient", 
                "setup": [
                    [
                        "$(@1 @3 % @2 @4 %)", 
                        "$(@1 @5 % @2 @6 %)"
                    ],
                    [
                        "$(@7 @8 @9)",
                        "$(@7 @10 % @8 @10 % @9 @10 %)"
                    ],
                    "polar"
                ]
            },
            {
                "type": "plugin", 
                "name": "random", 
                "at": "modify_fill_color", 
                "method": "randomize_color", 
                "arguments": [
                    [100, 1]
                ]
            },
            {
                "type": "saver", 
                "name":"inkscape-png"
            }
        ]
    }
]
```
A PNG can be generated from this using the command
```
python polygon.py delaunay_placeholder -o examples/ph_example.png 1920 1080 50 10 90 90 80 20 150 50 
```
This has generated the following image
![](examples/ph_example.png)
The arguments `1920 1080 50 10 90 90 80 20 150 50` can be split into the following parts:
- `1920 1080` is the resolution. These values are accessed with `@1` and `@2` in `"drawing_size"` and the painter setup
- `50 10` accessed by `@3` and `@4` are percentages for placing the center pole. 
The expression in the painter setup `"$(@1 @3 % @2 @4 %)"` expands to `"$(1920 50 % 1080 10 %)"`, which when evaluated is to values, the first being half of the image width and the second a tenth of the image height, which could have manually been written as `[960, 108]`.
- `90 90` accessed by `@5` and `@6` in the painter setup just like above, but for specifing a point on the radius of the polar gradient. This is at 90% of the width and height, so in the lower right corner at pixel `[1728, 972]`
- `80 20 150` is the RGB value of the pole. They are argument `@7`, `@8`, and `@9` and accessed in the list of colors in the painter setup. 
- and lastly `50` accessed as `@10`. This is used for the secondary color, which gets calculated as 50% of the primary color. The expression in the painter setup is `"$(@7 @10 % @8 @10 % @9 @10 %)"` which expands to `"$(80 50 % 20 50 % 150 50 %)"` and evaluates to `[40, 10, 75]`

## Support
Please use the Gitlab issue tracker for support.

## Roadmap
Fun things that can be implemented later:
- voronoi diagram plotter plugin
- Plugin development documentation

## Contributing
If you have anything to contribute like a cool plugin, you can submit a merge request. 

## Authors and acknowledgment
Created by [Tobias Frejo](https://gitlab.com/frejo).

## License
The software in this repository is licensed under the [MIT License](LICENSE).
