import random
import colorsys
import logging
logger = logging.getLogger(__name__)

class Plugin:
    def __init__(self) -> None:
        logger.info("Random plugin loaded")
    
    def randomize_color(self, rgb, rand:tuple[int]):
        # Get the pixel color and convert to HLS
        r,g,b = rgb
        h,l,s = colorsys.rgb_to_hls(r/255, g/255, b/255) 

        if len(rand) == 2:
            hue_chance = rand[0]
            lightness_chance = rand[0]
            hue_variance = rand[1]
            lightness_variance = rand[1]
        elif len(rand) == 4:
            hue_chance = rand[0]
            lightness_chance = rand[1]
            hue_variance = rand[2]
            lightness_variance = rand[3]
        
        if hue_chance > 100:
            hue_chance = 100
        if lightness_chance > 100:
            lightness_chance = 100
        
        # Add a little noise to hue and lightness if randomness is applied
        if hue_chance > 0 and random.randrange(100) < hue_chance:
            h = h + random.randint(-hue_variance, hue_variance)/100.0
            if h < 0:
                h = abs(h) % 1
            elif h > 1:
                h = h - abs(h) % 1

        if lightness_chance > 0 and random.randrange(100) < lightness_chance:
            l = l + random.randint(-lightness_variance, lightness_variance)/100.0
            if l < 0:
                l = abs(l) % 1
            elif l > 1:
                l = l - abs(l) % 1
        
        # Convert back to RGB
        r,g,b = colorsys.hls_to_rgb(h,l,s)

        # Go from float to 8 bit values
        r = int(r*255)
        g = int(g*255)
        b = int(b*255)

        return r,g,b
