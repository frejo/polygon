from PIL import Image

import logging
logger = logging.getLogger(__name__)

class Painter:
    img : Image.Image

    def __init__(self) -> None:
        logger.info("PNG Painter loaded")
    
    def setup(self, src_file):
        self.img = Image.open(src_file)

    def paint(self, xy:tuple[int,int]):
        # If coordinate exceeds the image, just use the nearest edge
        x,y = xy
        if x > self.img.width:
            xy[0] = self.img.width
        elif x < 0:
            xy[0] = 0
        if y > self.img.height:
            xy[1] = self.img.height
        elif y < 0:
            xy[1] = 0

        # Get the pixel color and convert to HLS
        # Pillow uses 8 bit values [0:255] while colorsys uses floats [0.0:1.0] 
        pixel = self.img.getpixel(xy)
        r,g,b = pixel[0:3]

        return r,g,b