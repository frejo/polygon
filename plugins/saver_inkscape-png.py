import subprocess
import svgwrite
import tempfile
import shutil
from os import path, makedirs
import logging
logger = logging.getLogger(__name__)

class Saver:
    def __init__(self) -> None:
        logger.info("Inkscape PNG saver loaded")
    
    def save(self, drawing:svgwrite.Drawing,  output='output.png', inkscape_path='C:\\Program Files\\Inkscape\\bin\\inkscape.exe'):

        tmpdir = tempfile.mkdtemp()
        svg_path = path.join(tmpdir, path.basename(output) + '.svg')

        if not path.exists(path.dirname(svg_path)):
            makedirs(path.dirname(svg_path))
        drawing.saveas(svg_path)

        ret = subprocess.run([inkscape_path, '--export-type=png', f'--export-filename={output}', svg_path], shell=True, capture_output=True)

        logger.debug('Inkscape export return: %s', ret)

        shutil.rmtree(tmpdir)

