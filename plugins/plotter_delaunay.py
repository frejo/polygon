from scipy import spatial
import random
import logging
logger = logging.getLogger(__name__)

class Plotter:
    def __init__(self) -> None:
        logger.info("Delaunay plotter loaded")
    
    def random_points(self, width, height, numpoints, force_corners=None, padding:int=0):
        points = []
        random.seed(None)
        for i in range(numpoints):
            points.append((
                random.randint(-padding, width+padding),
                random.randint(-padding, height+padding)
            ))

        if force_corners:
            corners = [(-padding,-padding), (width+padding, -padding), (-padding, height+padding), (width+padding, height+padding)]
            for c in corners:
                if not c in points:
                    points.append(c)
        
        return points
    
    def plot(self, width:int, height:int, num:int, padding:int=0) -> list[tuple]:
        p = self.random_points(width, height, num, force_corners=True, padding=padding)
        tri = spatial.Delaunay(p)

        triangles = []

        for t in tri.simplices:
            triangles.append((p[t[0]], p[t[1]], p[t[2]]))

        return triangles