from scipy.spatial import distance
import numpy as np
from bisect import bisect

import logging
logger = logging.getLogger(__name__)

class Painter:
    is_setup = False
    points:list[tuple[int,int]]
    colors:list[tuple[int,int,int]]
    point_col:list[tuple[tuple[int,int],tuple[int,int,int]]]
    gradient : str
    locations: list[str]

    def __init__(self) -> None:
        self.start = (0,0)
        self.end = (100, 100)
        self.is_setup = False

        logger.info("Gradient Painter loaded")
    
    def setup(self, points:list[tuple[int,int]], colors:list[tuple[int,int,int]], gradient='polar'):
        gradient = gradient.lower()

        if gradient == 'polar' or gradient == 'poles':
            if not (points is None or colors is None) and not len(points) == len(colors):
                raise Exception(f"Theres is not the same number of points and colors given: {len(points)} vs {len(colors)}")
            self.point_col = list(zip(points, colors))

            self.points = points
            self.colors = colors
        
        elif gradient == 'linear':
            if not (points is None or colors is None) and not len(points) == len(colors)+2:
                raise Exception(f"Linear gradient requires there are two more points than number of colors: {len(points)} points vs {len(colors)} colors")
            
            # Linear gradient requires setup of 2 + n points with n colours
            # The first two points describe the vector of the linear gradient
            # The rest of the points maps to the colours

            self.points = points[0:2]

            start = np.array(points[0])
            end = np.array(points[1])

            locations = []

            for p in points[2:]:
                # Calculate how far from the start point, projected
                # onto the line between the start and end, each point is
                p3 = np.array(p)

                t = self._length_along_line(start, end, p3)

                locations.append(t)
            

            logger.debug("Colours: %s", colors)
            logger.debug("Locations: %s", locations)
            colloc = list(zip(colors, locations))
            colloc.sort(key=lambda tup: tup[1])
            logger.debug("Colour points and locations sorted: %s", colloc)
            self.colors, self.locations = tuple(zip(*colloc))

        else:
            raise Exception("Unknown gradient type " + gradient)


        self.gradient = gradient

        self.is_setup = True

    def paint(self, xy):
        if not self.is_setup:
            raise Exception("Painter.setup() has to be run before Painter.paint()")
        
        match self.gradient:
            case 'polar':
                return self._polar(xy)
            case 'poles':
                return self._poles(xy)
            case 'linear':
                return self._linear(xy)

    def _poles(self, xy):
        # Inverse Distributed Weighing 
        # https://www.geo.fu-berlin.de/en/v/soga/Geodata-analysis/geostatistics/Inverse-Distance-Weighting/index.html
        rgb = np.array((0,0,0), dtype=float)
        
        n = len(self.points)

        β = 1

        w = []
        for p in self.points:
            w.append(distance.euclidean(xy, p)**(-β))

        w_sum = sum(w)

        for i in range(n):
            frac = w[i]/w_sum
            rgb += np.array(self.colors[i])*frac
        
        r,g,b = tuple( rgb )

        return int(r), int(g), int(b)

    def _polar(self, xy):
        if not len(self.points) == 2:
            raise Exception("Polar gradient requires 2 coordinates and 2 colors")

        dist1 = distance.euclidean(xy, self.points[0])
        dist2 = distance.euclidean(self.points[0], self.points[1])

        frac = dist1/dist2
        if frac > 1.0:
            frac = 1.0

        r,g,b = tuple( np.array(self.colors[0])*(1-frac) + np.array(self.colors[1])*frac )
        return int(r), int(g), int(b)

    def _linear(self, xy):
        start = np.array(self.points[0])
        end = np.array(self.points[1])
        p = np.array(xy)

        t = self._length_along_line(start, end, p)

        c1 = None
        c2 = None

        l = bisect(self.locations, t)
        if l == 0:
            c1 = self.colors[0]
        elif l == len(self.locations):
            c1 = self.colors[-1]
        else:
            c1 = self.colors[l-1]
            t1 = self.locations[l-1]
            c2 = self.colors[l]
            t2 = self.locations[l]
        
        #logger.debug("Location %s projects to %s, with colors %s and %s", xy, t, c1, c2)
        
        if c2 is None and not c1 is None:
            return c1
        elif not c2 is None and not c1 is None:
            l1 = t-t1
            l2 = t2-t

            norm = l1+l2
            l1 /= norm
            l2 /= norm


            c = np.array(c2)*l1 + np.array(c1)*l2

            r,g,b = tuple(c)
            #logger.debug("Final color: %s", (r,g,b))
            return int(r), int(g), int(b)

        else:
            raise Exception("System error. This should in theory never happen.")
        
    def _length_along_line(self, a, b, p):
        v = b-a

        proj = np.dot(p, v)/np.dot(v,v) * v
        l = np.linalg.norm(proj)

        #logger.debug("v: %s, proj: %s", v, proj)
        if np.array_equal(np.sign(v), np.sign(proj)):
            return l
        else:
            return -l
        
