import logging
logger = logging.getLogger(__name__)

class Plotter:
    def __init__(self) -> None:
        logger.info("Right Triangle plotter loaded")
    
    def plot(self, width:int, height:int, column_width:int, row_height:int) -> list[tuple]:
        X,Y = column_width, row_height
        polygons = []
        for i in range(0, width, X):
            for j in range(0, height, Y):
                if j/Y % 2 == 0:
                    p1 = ((i, j), (i+X, j), (i, j+Y))
                    p2 = ((i+X, j), (i, j+Y), (i+X, j+Y))
                else:
                    p1 = ((i, j), (i+X, j), (i+X, j+Y))
                    p2 = ((i, j), (i, j+Y), (i+X, j+Y))
                polygons.append(p1)
                polygons.append(p2)

                #print(i,j)
                #print(p1, p2)
        return polygons