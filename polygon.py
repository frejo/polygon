import importlib
import os
import logging
import inspect
import svgwrite
import json
import argparse

logger = logging.getLogger(__name__)

class PolygonApplication:
    extra_args:list

    def __init__(self, toolchains='toolchains.json', extra_args=None):
        logger.info("Starting the application")
        self.output = "polygon.out"
        self.load_plugins()
        self.load_toolchains(toolchains)

        if not extra_args is None:
            self.extra_args = extra_args
    
    def __del__(self):
        logger.info("Application ended")

    def parse_placeholder(self, s:str):
        if s.startswith('$(') and s.endswith(')'):
            expr = s[2:-1]
            vals = expr.split(' ')
            
            # Queue elements for postfix parsing 
            # Needed to replace @ placeholders and convert numbers
            queue = []

            for val in vals:
                try:
                    val = float(val)
                    if val.is_integer():
                        val = int(val)
                except ValueError:
                    if val.startswith('@'):

                        try:
                            idx = int(val[1:]) - 1     # Placeholder @1 should be the 0'th index
                            val = self.extra_args[idx]
                            try: 
                                val = float(val)
                                if val.is_integer():
                                    val = int(val)
                            except:
                                pass
                                
                        except ValueError:
                            raise Exception("@ in placeholder could not be parsed to give an index value.")
                        except IndexError:
                            raise Exception("Not enough args given.")
                finally:
                    queue.append(val)
            
            # Stack to actually handling the parsing
            stack = []
            while len(queue) > 0:
                val = queue.pop(0)
                if val in ['+', '-', '*', '/', '%', '%f']:
                    b = stack.pop()
                    a = stack.pop()

                    match val:
                        case '+':
                            stack.append(a+b)
                        case '-':
                            stack.append(a-b)
                        case '*':
                            stack.append(a*b)
                        case '/':
                            stack.append(a/b)
                        case '%':
                            stack.append(int(a*b/100))
                        case '%f':
                            stack.append(a*b/100)
                else:
                    stack.append(val)
            
            return(tuple(stack))
        return s
    
    def _replace_str(self, s:str):
        x = self.parse_placeholder(s)
        s_new = s
        if not x is None:
            if len(x) == 1:
                s_new = x[0]
            else:
                s_new = x

        if not s == s_new:
            logger.debug("Replaced %s with %s", s, s_new)
        
        return s_new

    def _replace_list(self, l:list):
        x = []
        for e in l:
            if isinstance(e, list):
                e = self._replace_list(e)
            elif isinstance(e, dict):
                e = self._replace_dict(e)
            elif isinstance(e, str):
                e = self._replace_str(e)
            x.append(e)
        return x       

    def _replace_dict(self, d:dict):
        x = {}
        for k,v in d.items():
            if isinstance(v, list):
                v = self._replace_list(v)
            elif isinstance(v, dict):
                v = self._replace_dict(v)
            elif isinstance(v, str):
                v = self._replace_str(v)
            x[k] = v
        return x

    def replace_placeholders(self, element):
        if isinstance(element, dict):
            return self._replace_dict(element)
        elif isinstance(element, tuple):
            return self._replace_list(element)
        elif isinstance(element, str):
            return self._replace_str(element)
        return element

    def load_plugins(self):
        logger.info("Start loading plugins")
        
        mypath = 'plugins'
        plugins = [] #f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f)) and f.split('.')[-1] == 'py' and not f == '__init__.py']

        plugins_folder = os.listdir(mypath)
        logger.debug("Files in the plugins folder: %s", plugins_folder)

        for f in plugins_folder:
            if os.path.isfile(os.path.join(mypath, f)) and f.endswith('.py') and not f == '__init__.py':
                plugins.append(f[0:-3])

        logger.debug("Found the following plugins: %s", plugins)

        self._plugins = {}
        self._plotters = {}
        self._painters = {}
        self._savers = {}

        for plugin in plugins:
            logger.info("Loading plugin %s.", plugin)
            # Import the module and initialise it
            module = importlib.import_module(f"plugins.{plugin}")
            classes = []
            for name, obj in inspect.getmembers(module):
                if inspect.isclass(obj):
                    classes.append(name)
            logger.debug("Plugin %s implements %s", plugin, classes)

            if 'Plugin' in classes:
                if plugin.startswith('plugin_'):
                    self._plugins[plugin[7:]] = module.Plugin()
                else:
                    self._plugins[plugin] = module.Plugin()
            if 'Plotter' in classes:
                if plugin.startswith('plotter_'):
                    self._plotters[plugin[8:]] = module.Plotter()
                else:
                    self._plotters[plugin] = module.Plotter()
            if 'Painter' in classes:
                if plugin.startswith('painter_'):
                    self._painters[plugin[8:]] = module.Painter()
                else:
                    self._painters[plugin] = module.Painter()
            if 'Saver' in classes:
                if plugin.startswith('saver_'):
                    self._savers[plugin[6:]] = module.Saver()
                else:
                    self._savers[plugin] = module.Saver()
        
        logger.debug("The following Plugins have been loaded: %s", 
            ', '.join(list(self._plugins.keys())))
        logger.debug("The following Plotters have been loaded: %s", 
            ', '.join(list(self._plotters.keys())))
        logger.debug("The following Painters have been loaded: %s", 
            ', '.join(list(self._painters.keys())))
        logger.debug("The following Savers have been loaded: %s", 
            ', '.join(list(self._savers.keys())))
    
    def load_toolchains(self, toolchains_file):
        with open(toolchains_file) as fp:
            tc = json.load(fp)

            self.toolchains = {}
            for d in tc:
                self.toolchains[d['name']] = d

        logger.info("Loaded %s toolchain(s): %s", len(self.toolchains), ', '.join(self.toolchains.keys()))

    def run(self, toolchain_name):
        logger.info("Running")

        """ Stages:
            setup
            pre-plotting
            plotting - Plotter
            modyfy_polygons

            svg_setup
            modify_drawing_pre_paint

            painting
                paint_polygon
                modify_fill_color
                modify_fill
                modify_stroke
                modify_painted_polygon

            modify_drawing_post_paint
            save
        """

        if not toolchain_name in self.toolchains.keys():
            raise Exception("Unknown toolchain " + toolchain_name)

        # ---- SETUP ---- #
        toolchain = self.toolchains[toolchain_name]

        toolchain = self.replace_placeholders(toolchain)

        if 'drawing_size' in toolchain.keys():
            drawing_size = toolchain['drawing_size']
        elif 'size_from_painter' in toolchain.keys():
            drawing_size = (-1,-1)
        else:
            raise Exception('No drawing size specified. Please add "drawing_size" or "size_from_painter" to the toolchain.')
        
        plotter = None
        painter = None
        savers = {}
        plugins = {}

        for tool in toolchain['chain']:
            if tool['type'] == "plotter":
                if tool['name'] in self._plotters.keys():
                    plotter = self._plotters[tool['name']]
                    plotter_args = tool['arguments']
                else:
                    raise Exception("Not plotter plugin loaded called " + tool['name'])
            elif tool['type'] == "painter":
                if tool['name'] in self._painters.keys():
                    painter = self._painters[tool['name']]
                    painter_setup = tool['setup']
                else:
                    raise Exception("Not painter plugin loaded called " + tool['name'])
            elif tool['type'] == "saver":
                if tool['name'] in self._savers.keys():
                    savers[tool['format']] = self._savers[tool['name']]
                else:
                    raise Exception("Not saver plugin loaded called " + tool['name'])
            elif tool['type'] == "plugin":
                if tool['name'] in self._plugins.keys():
                    if not tool['at'] in plugins.keys():
                        plugins[tool['at']] = []

                    plugins[tool['at']].append((
                        self._plugins[tool['name']],
                        tool['method'],
                        tool['arguments']
                    ))
                else:
                    raise Exception("Not plugin loaded called " + tool['name'])

        size_from_painter = painter.setup(*painter_setup)
        if drawing_size == (-1,-1):
            if isinstance(size_from_painter, tuple) and len(size_from_painter) == 2:
                drawing_size = size_from_painter
            else:
                raise Exception("Painter was expected to return the drawing size. It returned " + size_from_painter)

        # ---- PRE-PLOTTING ---- #

        # ---- PLOTTING ---- #
        polygons = plotter.plot(*drawing_size, *plotter_args)

        # ---- MODIFY_POLYGONS ---- #
        if 'modify_polygons' in plugins.keys():
            for plugin, method_name, args in plugins['modify_polygons']:
                if hasattr(plugin, method_name):
                    polygons = getattr(plugin, method_name)(polygons, *args)

        # ---- SVG_SETUP ---- #
        dwg = svgwrite.Drawing(size=tuple(drawing_size))
        shapes = dwg.add(dwg.g(id="polygons"))

        # ---- MODIFY_DRAWING_PRE_PAINT ---- #
        if 'modify_drawing_pre_paint' in plugins.keys():
            for plugin, method_name, args in plugins['modify_drawing_pre_paint']:
                if hasattr(plugin, method_name):
                    dwg = getattr(plugin, method_name)(dwg, *args)

        # ---- PAINTING ---- #
        for points in polygons:
            x, y = 0,0
            for t in points:
                x += t[0]
                y += t[1]
            x = int(x/len(points))
            y = int(y/len(points))

            polygon = shapes.add(dwg.polygon(points))

            # ---- PAINT_POLYGON ---- #
            rgb = painter.paint((x,y))

            # ---- MODIFY_FILL_COLOR ---- #
            if 'modify_fill_color' in plugins.keys():
                for plugin, method_name, args in plugins['modify_fill_color']:
                    if hasattr(plugin, method_name):
                        rgb = getattr(plugin, method_name)(rgb, *args)

            # ---- MODIFY_FILL ---- #
            fill = {'color': f'rgb{rgb}'}

            if 'modify_fill' in plugins.keys():
                for plugin, method_name, args in plugins['modify_fill']:
                    if hasattr(plugin, method_name):
                        fill = getattr(plugin, method_name)(fill, *args)

            polygon.fill(**fill)

            # ---- MODIFY_STROKE ---- #
            stroke = {'color': f'rgb{rgb}', 'width': 1}

            if 'modify_stroke' in plugins.keys():
                for plugin, method_name, args in plugins['modify_stroke']:
                    if hasattr(plugin, method_name):
                        stroke = getattr(plugin, method_name)(stroke, *args)

            polygon.stroke(**fill)

            # ---- MODIFY_PAINTED_POLYGON ---- #
            if 'modify_painted_polygon' in plugins.keys():
                for plugin, method_name, args in plugins['modify_painted_polygon']:
                    if hasattr(plugin, method_name):
                        getattr(plugin, method_name)(polygon, *args)
        
        # ---- MODIFY_DRAWING_POST_PAINT ---- #
        if 'modify_drawing_post_paint' in plugins.keys():
            for plugin, method_name, args in plugins['modify_drawing_post_paint']:
                if hasattr(plugin, method_name):
                    getattr(plugin, method_name)(dwg, *args)

        # ---- SAVE ---- #
        for ext, saver in savers.items():
            filename = f"{self.output}.{ext}"
            saver.save(dwg, filename)

    
    def test(self):
        saver = self._savers['inkscape-png']
        rand = self._plugins['random']

        plotter = self._plotters['right']
        painter = self._painters['png']
        polys = plotter.plot(400, 400, 20, 20)
        painter.setup('_test/400.png')
        dwg = svgwrite.Drawing(size=(400, 400))

        #plotter = self._plotters['delaunay']
        #painter = self._painters['gradient']
        #polys = plotter.plot(1920, 1080, 200)
        #painter.setup((222,0,150), (0,150,150), (400,200), (1520,880), 'poles')
        #dwg = svgwrite.Drawing(size=(1920, 1080))

        shapes = dwg.add(dwg.g(id="shapes"))

        for points in polys:
            x, y = 0,0
            for t in points:
                x += t[0]
                y += t[1]
            x = int(x/len(points))
            y = int(y/len(points))

            polygon = shapes.add(dwg.polygon(points))
            col = painter.paint((x,y))
            col = rand.randomize_color(col, (100, 2))
            polygon.fill(f'rgb{col}')
            polygon.stroke(f'rgb{col}', 1)
        
        saver.save(dwg, '_test/400out.png')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('toolchain')
    parser.add_argument('-o', '--output', default='polygon.out')
    parser.add_argument('-c', '--toolchain_config', default='toolchains.json')
    parser.add_argument('-v', '--verbose', action='count', default=0)
    
    args, unknown = parser.parse_known_args()

    if args.verbose == 0:
        logging.basicConfig(level=logging.WARNING, format='[%(levelname)s] %(message)s')
    elif args.verbose == 1:
        logging.basicConfig(
            level=logging.INFO, 
            format='%(asctime)s [%(levelname)s] %(message)s',
            datefmt='%H:%M:%S'
        )
    elif args.verbose >= 2:
            logging.basicConfig(
                level=logging.DEBUG, 
                format='%(asctime)s [%(levelname)s] %(name)s : %(message)s'
            )
        
    logger.debug("Known args: %s", args)
    logger.debug("Unknown args: %s", unknown)

    app = PolygonApplication(args.toolchain_config, unknown)
    #app.test()
    app.output = args.output
    app.run(args.toolchain)

