# System Specification

Generation process
1. Generate polygons (Plotting plugin)
2. Create internal SVG file (core)
3. Add each polygon to the SVG and add a colour (core + Painting plugin)
4. Save (SVG) file


## Plugin system.

Plugins:
- Plotting (creating the mesh)
    - Takes dimensions and method specific arguments, e.g. node count / density
    - Returns a list of polygons. Each polygon is a list of its corners
- Painting
    - Initialised with parameters for colouring. Could be a file path or a colour value
    - Colour method then takes a coordinate pair and returns it's corresponding colour
- Saving
    - Save the image to the system, e.g. by converting from the SVG generated by the default plugin

Define tool chains in JSON config


Default plugins:
- Plotting
    - Right triangles
    - Delaunay
- Painting
    - From PNG
    - Random
    - Simple gradients
- Saving
    - SVG
    - SVG to PNG using Inkscape
