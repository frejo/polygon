import importlib
from polygon import PolygonApplication
import os

nodes = 300
sizes = [
    ((1920, 1080), "1080p"),
    ((2560, 1080), "1080p-UW"),
    ((2560, 1440), "QHD"),
    ((3440, 1440), "UWQHD"),
    ((3840, 2460), "4K"),
    ((7680, 4320), "8K"),
]
tools = [
    "pride",
    "ace",
    "aro",
    "bi",
    "pan",
    "trans"
]
tc = "pride.json"
outdir = "_output"
if not os.path.exists(outdir):
    os.makedirs(outdir)

def fname(tool, res): 
    p = os.path.join(outdir, f"{tool}_{res}")
    return p

for tool in tools:
    for size, name in sizes:
        print(tool, name)

        # Setup the generator. (*size, nodes) are the args passed to placeholders 
        app = PolygonApplication(tc, (*size, nodes))
        app.output = fname(tool, name)
        app.run(tool)